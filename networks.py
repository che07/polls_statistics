# import pexpect
import csv
import getpass
import bcrypt

# output = pexpect.run("ls -la /Users/che/che", encoding='utf-8')
#
# print(output)

# ---------------------------------------------------------------------------
'''
Создать функцию write_dhcp_snooping_to_csv, 
которая обрабатывает вывод команды show dhcp snooping binding из разных файлов 
и записывает обработанные данные в csv файл.
Аргументы функции:
filenames - список с именами файлов с выводом show dhcp snooping binding
output - имя файла в формате csv, в который будет записан результат
'''


def write_dhcp_snooping_to_csv(*filenames, output_file: str):
    res_data = [["switch", "mac", "ip", "vlan", "interface"]]

    for i in filenames:
        try:
            with open(i, 'r') as f:
                for line in f:
                    if 'dhcp-snooping' in line:
                        line_list = line.split()
                        res_data.append([i[:3], line_list[0], line_list[1], line_list[4], line_list[-1]])
        except FileNotFoundError:
            print(f"No such file or directory: {i}")
            pass
        else:
            with open(output_file, 'w') as f:
                writer = csv.writer(f)  # quoting=csv.QUOTE_NONNUMERIC
                for row in res_data:
                    writer.writerow(row)
    return "OK\n"


print(write_dhcp_snooping_to_csv('sw3_dhcp_snooping.txt',
                                 'sw6_dhcp_snooping.txt',
                                 'dhcp2_snooping.txt',
                                 output_file='test.csv'))
# ---------------------------------------------------------------------------
'''
Запрос ввода login/pass от пользователя, без отображения вводимого пароля
и запись данных в файл
'''
entitie = {}

login_ = input("Login: \n")

with open('passwd.txt', 'r') as f:
    for line in f:
        if login_ in line:
            raise "Sorry, that's user already exist"

entitie[login_] = bcrypt.hashpw(getpass.getpass(prompt='Password: ', stream=None).encode('utf-8'), bcrypt.gensalt())
with open('passwd.txt', 'a') as f:
    for k, v in entitie.items():
        f.write(f"{k} - {v}\n")

print(entitie)
# ---------------------------------------------------------------------------
