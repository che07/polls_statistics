import random

b = random.sample([i for i in range(0, 10)], 7)
print(b)

for i in range(len(b)-1):
    for j in range(len(b)-i-1):
        if b[j]>b[j+1]:
           b[j], b[j+1] = b[j+1], b[j]



try:
    print(f'index of integer "5" is {b.index(5)}')
except Exception as e:
    print(f"\n== EXCEPTION ==\n{e}\n")
finally:
    print(f'Sorted list is: {b}')