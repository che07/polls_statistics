import datetime
# import time

b = datetime.datetime.now().replace(microsecond=0) # удаляем милисекунды '2022-07-19 17:48:44.932323'
print(b)

dt = datetime.datetime.strptime('2022-06-28 13:00:00', '%Y-%m-%d %H:%M:%S') # из строки '2022-07-19 17:48:44' в формат datetime
print(dt)

print(int(dt.timestamp())) # из datetime.datetime ('2022-07-19 17:48:44') в unixtime ('1658242124')