# Polls STATISTICS API
@router.get('/statistics/{poll_id}')
def get_all_data(poll_id: int):
    """
    Возвращает словарь из типов опросов (group/question/rating) и их данных (title - question, value - count of values)
    (обычно, в опросе используется только ОДИН тип (group/question/rating),
    но в БД нашелся опрос с ДВУМЯ типами (group & rating)) сразу
    """
    poll_stat = PollResultEntity.select().where(PollResultEntity.poll_data_id == poll_id)
    res = [PollResultDTO.from_orm(i) for i in poll_stat]

    rating_value, rating_title, rating_feedback = [], [], []
    try:
        dd = defaultdict(str)
        for i in res:
            try:
                json.loads(i.result)
            except Exception as e:
                print(f'| EXCEPTION in "polls_controller" | poll_data_id: {i.id} |\n MESSAGE: < {e} > |')
            else:
                if json.loads(i.result)[0][0].get('result') == 'group':
                    for v in json.loads(i.result)[0]:
                        dd[v['title']] += (v['value'] + '&&%')
                elif json.loads(i.result)[0][0].get('result') == 'rating':
                    rating_title.append(json.loads(i.result)[0][0].get('title'))
                    rating_value.append(json.loads(i.result)[0][0].get('value'))
                    # if json.loads(i.result)[0][0].get('feedback') != '':
                    #     rating_feedback.append(json.loads(i.result)[0][0].get('feedback'))
    except Exception as e:
        print(f'EXCEPTION:\n\t{e}')
        res_dict = {}
    else:
        group_res_list = []
        for k, v in dd.items():
            group_res_list.append({'title': k, 'value': dict(Counter(v.split('&&%')[:-1]))})

        if rating_value:
            res_dict = {'group': group_res_list,
                        'rating': {'title': ''.join(set(rating_title)),
                                   'value': dict(Counter([i for i in rating_value]))}}
        else:
            res_dict = {'group': group_res_list,
                        'rating': []}

    return res_dict