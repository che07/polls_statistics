from tkinter import CENTER
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Alignment, Font

data = [
    {
        "client_mac": "20:82:6A:D7:62:05",
        "phone": "******3698",
        "user_name": "",
        "LogTime": "2022-07-01 23:25",
        "social_net": "pass",
        "user_id": "#",
        "type": "skip",
        "allow_spam": 0,
        "partner_keyid": "1PpjBzSW",
        "count": "44",
        "language": "ru",
        "user_agent": "Mozilla/5.0 (Linux; Android 9; CPH2083) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36",
        "gender": "Другой",
        "bdate": "",
        "title": "Центральный Детский Магазин на Лубянке",
        "device": "Android",
        "age": "Нет данных",
        "OS": "Android"
    },
    {
        "client_mac": "A2:A2:35:E4:15:4C",
        "phone": "******7577",
        "user_name": "Петр Барыгин",
        "LogTime": "2022-07-01 21:46",
        "social_net": "vk",
        "user_id": "https://vk.com/id452253932",
        "type": "join",
        "allow_spam": 0,
        "partner_keyid": "kNPjJ5ku",
        "count": "3",
        "language": "ru",
        "user_agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 15_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148",
        "gender": "Мужчина",
        "bdate": "14.11.1902",
        "title": "Универмаг \"Цветной\"",
        "device": "iPhone",
        "age": "120",
        "OS": "iOS"
    }]

# Создаем WorkBook exel и подготавливаем его
wb = Workbook()
ws = wb.create_sheet('Пользователи', 0)  # задаем название и делаем активным созданный лист
ws.sheet_properties.tabColor = "ff5024"  # цвет листа
a = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']
for i in a:
    ws.column_dimensions[i].width = 40
    

# Пишем в файл сводную информацию
xml_headers = ['Последний визит',
                'Тип авторизации',
                'Имя',
                'Ссылка',
                'Посещений',
                'Доступ',
                'Возраст',
                'Пол',
                'Языки устройств',
                'День рождения',
                'ОС устройств',
                'Устройство',
                'Посещенные точки',
                'Телефон',
                'СМС-рассылка']
for indx, val in enumerate(xml_headers):
    ws.cell(row=1, column=indx + 1).value = val  # Пишем названия столбцов
    ws.cell(row=1, column=indx + 1).fill = PatternFill(fill_type='solid', fgColor='ff5024')  # Раскрашиваем ячейки
    ws.cell(row=1, column=indx + 1).alignment = Alignment(horizontal=CENTER)
    ws.cell(row=1, column=indx + 1).font = Font(bold=True, color='FFFFFFFF')

# Теперь пишем в файл стату
row_detail = 2
for i in data:
    ws.cell(row=row_detail, column=1).value = i.get('LogTime')
    ws.cell(row=row_detail, column=2).value = i.get('social_net')
    ws.cell(row=row_detail, column=3).value = i.get('user_name') if i.get('user_name') else i.get('phone')
    ws.cell(row=row_detail, column=4).value = i.get('user_id')
    ws.cell(row=row_detail, column=5).value = i.get('count')
    ws.cell(row=row_detail, column=6).value = i.get('type')
    ws.cell(row=row_detail, column=7).value = i.get('age')
    ws.cell(row=row_detail, column=8).value = i.get('gender')
    ws.cell(row=row_detail, column=9).value = i.get('language')
    ws.cell(row=row_detail, column=10).value = i.get('bdate') if i.get('bdate') else 'Не указано'
    ws.cell(row=row_detail, column=11).value = i.get('OS')
    ws.cell(row=row_detail, column=12).value = i.get('device')
    ws.cell(row=row_detail, column=13).value = i.get('title')
    ws.cell(row=row_detail, column=14).value = i.get('phone')
    ws.cell(row=row_detail, column=15).value = 'Да' if i.get('allow_spam') == 1 else 'Нет'
    
    c = 1
    while c < 16:
        ws.cell(row=row_detail, column=c).alignment = Alignment(horizontal=CENTER)
        c += 1

    row_detail += 1

wb.save('test.xlsx')
wb.close()