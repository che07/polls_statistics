import time

import pygame as p
import random
from pygame.locals import *

# Константы цветов RGB
BLACK = (0 , 0 , 0)
WHITE = (255 , 255 , 255)
# Создаем окно
root = p.display.set_mode((1120 , 914))
# 2х мерный список с помощью генераторных выражений
cells = [[random.choice([0 , 1]) for j in range(root.get_width() // 8)] for i in range(root.get_height() // 8)]


# Функция определения кол-ва соседей
def near(pos: list , system=[[-1 , -1] , [-1 , 0] , [-1 , 1] , [0 , -1] , [0 , 1] , [1 , -1] , [1 , 0] , [1 , 1]]):
    count = 0
    for i in system:
        if cells[(pos[0] + i[0]) % len(cells)][(pos[1] + i[1]) % len(cells[0])]:
            count += 1
    return count


# Основной цикл
while 1:
    # Заполняем экран белым цветом
    root.fill(WHITE)

    # Рисуем сетку
    for i in range(0 , root.get_height() // 3):
        p.draw.line(root , BLACK , (0 , i * 10) , (root.get_width() , i * 10))
    for j in range(0 , root.get_width() // 3):
        p.draw.line(root , BLACK , (j * 10 , 0) , (j * 10 , root.get_height()))
    
    # Нужно чтобы виндовс не думал что программа "не отвечает"
    for i in p.event.get():
        if i.type == QUIT:
            quit()
    
    # Проходимся по всем клеткам
    for i in range(0 , len(cells)):
        for j in range(0 , len(cells[i])):
            print(cells[i][j],i,j)
            if cells[i][j] == 4:
                p.draw.rect(root , (200 * cells[i][j] % 256 , 100 , 22) , [i * 10 , j * 10 , 20 , 20])
            else:
                p.draw.rect(root , (148 if cells[i][j] == 1 else 200 * cells[i][j] % 256 , 148 if int(i) > 113 else 13 , 22) , 
                [i * 10 , j * 10 , 10 , 10])
    
    # Обновляем экран
    p.display.update()
    cells2 = [[0 for j in range(len(cells[0]))] for i in range(len(cells))]
    for i in range(len(cells)):
        for j in range(len(cells[0])):
            if cells[i][j]:
                if near([i , j]) not in (2 , 3):
                    cells2[i][j] = 0
                    continue
                cells2[i][j] = 1
                continue
            if near([i , j]) == 3:
                cells2[i][j] = 1
                continue
            cells2[i][j] = 0
    cells = cells2