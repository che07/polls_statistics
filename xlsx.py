from openpyxl import Workbook
from openpyxl.styles import PatternFill, Alignment, Font
from tkinter import CENTER


cells = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M']


wb = Workbook()
ws = wb.create_sheet('test', 0)  # задаем название листа
ws.sheet_properties.tabColor = "ff5025"  # цвет листа
for i in cells:
    ws.column_dimensions[i].width = 2

row_ = 1
for cell in range(1, 14):
    ws.cell(row=row_, column=cell).fill = PatternFill(fill_type='solid', fgColor='ff5024')  # Раскрашиваем ячейки
    ws.cell(row=row_, column=cell + 1 if cell + 1 < 14 else cell).fill = PatternFill(fill_type='solid', fgColor='ff5024')  # Раскрашиваем ячейки
    row_ += 1

wb.save('life.xlsx')