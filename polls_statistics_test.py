import json
import datetime
from collections import defaultdict, Counter


print(f'\n=== {str(datetime.datetime.now())[:19]} ===\n')

a = "[[{\"title\":\"Вы уже стали участником нашей программы лояльности?\",\"value\":\"Да\",\"result\":\"group\"},{\"title\":\"Who?\",\"value\":\"NO\"}]]"
b = "[[{\"title\":\"Вы уже стали участником нашей программы лояльности?\",\"value\":\"Нет\",\"result\":\"group\"},{\"title\":\"Who?\",\"value\":\"Yes\"}]]"
c = "[[{\"title\":\"Вы уже стали участником нашей программы лояльности?\",\"value\":\"Да\",\"result\":\"group\"},{\"title\":\"Who?\",\"value\":\"NO\"},{\"title\":\"Придете снова?\",\"value\":\"Обязательно!!!\"}]]"
res = [a, b, c]

dd = defaultdict(str)
for i in res:
    for v in json.loads(i)[0]:
        dd[v['title']] += (v['value']+'&&%')
    
# print(dd)
res_dict = {}
for k,v in dd.items():
    print(v.split('&&%'))
    # print(v)
    res_dict[k] = dict(Counter(v.split('&&%')[:-1]))

print(res_dict)

# for git 